import java.util.InputMismatchException;
import java.util.Scanner;
public class Oldmaid{
	public static void main(String [] args){
		Scanner input = new Scanner(System.in);
		int amountOfPlayer = validateAmountOfPlayers(input);
		int participant = 0;
		//Creates a new deck
		Deck deck = new Deck();
		fancy(input, deck);
		deck.shuffle();
		DynamicPlayerArray players = createMultiplePlayers(amountOfPlayer,input,participant);
		distributeCardsToEachPlayers(deck, players); 
		play(input, participant, players);
	}
	//Start of the game
	private static void play(Scanner input, int participant, DynamicPlayerArray players) {
		boolean gameOver = false;
		while(!gameOver){
				System.out.println(players.getPlayer(participant).getName() + "'s hand \n"+players.getPlayer(participant));
				players.getPlayer(participant).discardPairs();
				System.out.println("Discarded cards \n" + players.getPlayer(participant).getName() + "'s hand \n"+players.getPlayer(participant));
				int cardToSteal = validatesTheUserInputForTheCardToSteal(input, players, participant);
				//Sets the current player to a variable. Sets the participant back to the first in order for the last player to be able to steal card from the next (first) player
				int currentParticipant = participant;
				if(participant >= players.getPlayerCount() -1){
					participant = -1;
				}
				stealCards(players.getPlayer(currentParticipant),players.getPlayer(participant + 1), cardToSteal);
				players.getPlayer(currentParticipant).discardPairs();
				gameOver = checksIfTheGameHasEndedAndTellsIfThereIsAWinner(players, gameOver, currentParticipant);
				if(participant >= players.getPlayerCount() -1){
					participant = -1;
				}
				participant++;
		}
	}
	//This method lets the user selected wether they want fancy cards or not
	private static void fancy(Scanner input, Deck deck) {
		System.out.println("Do you want to play with the fancy or normal cards?");
		String fancyResponse = input.nextLine();
		if(fancyResponse.equals("fancy")){ 
			for(int i=0;i<deck.sizeOfDeck();i++){
				deck.getCard(i).setFancy(true);
			}
		}
	}
	//This method checks if the game has ended and if there is a winner
	private static boolean checksIfTheGameHasEndedAndTellsIfThereIsAWinner(DynamicPlayerArray players, boolean gameOver, int currentParticipant) {
		if(players.getPlayer(currentParticipant).getAmountOfCards() == 0){
			System.out.println(players.getPlayer(currentParticipant).getName()+" has won!!");
			players.removePlayer(currentParticipant);
			System.out.println("There are now " + players.getPlayerCount() + " amount of players");
		}
		if(players.getPlayerCount() == 1){
			System.out.println("The loser is " + players.getPlayer(currentParticipant).getName());
			gameOver=true;
		}
		return gameOver;
	}
	//This method validates that the card that the user is trying to steal is in a valid range or that the input is a valid one.
	public static int validatesTheUserInputForTheCardToSteal(Scanner input, DynamicPlayerArray players, int participant) {
		boolean validCardIndex = false;
		int cardToSteal = 0;
		while(!validCardIndex){
			try{
				System.out.println("Which card would you like to steal? \n Enter a value from 0 to " + (players.getPlayer(participant).getAmountOfCards()-1));
				cardToSteal = Integer.parseInt(input.nextLine());
				if(cardToSteal < 0 || cardToSteal > players.getPlayer(participant).getAmountOfCards()-1){
					System.out.println("Please enter the correct range of value");					
				}
				else{
					System.out.println("Thank you for your input! We shall proceed further.");
					validCardIndex = true;
				}
			}
			catch (NumberFormatException error){
				System.out.println("Please enter numbers, not anything else!");
			}
		}
		return cardToSteal;
	}
	//This method equally distributes the cards
	public static void distributeCardsToEachPlayers(Deck deck, DynamicPlayerArray players) {
		int playerCount = 0;
		for(int i = 0;i<deck.sizeOfDeck();i++){
			if(playerCount >= players.getPlayerCount()){
				playerCount = 0;
			}
			players.getPlayer(playerCount).addCard(deck.getCard(i));
			playerCount++;
		}
	}
	//This method creates the multiple amount of players based on the valid amount of players
	public static DynamicPlayerArray createMultiplePlayers(int amountOfPlayer,Scanner input,int playerNumber) {
		DynamicPlayerArray players = new DynamicPlayerArray();
		for(int i = 0;i <amountOfPlayer;i++){
			System.out.println("Player " + (playerNumber + i + 1) + " please enter your name");
			players.addPlayer(new Player(input.nextLine()));
		}
		System.out.println("The player count is " + players.getPlayerCount());
		return players;
	}
	//This method validates that the user input is within a given valid range
	public static int validateAmountOfPlayers(Scanner input) {
		boolean validValue = false;
		int amountOfPlayer = 0;
		while(!validValue){
			try{
				System.out.println("Please enter the amount of players that will be playing the game. There has to be a minimum of 3 players and a max of 7!");
				amountOfPlayer = Integer.parseInt(input.nextLine()); // Catch and thrown exception if the input is not a int.
				if(amountOfPlayer < 3 || amountOfPlayer > 7){
					System.out.println("Enter the correct range of value");					
				}
				else{
					System.out.println("Thank you for your input! We shall proceed further.");
					validValue = true;
				}
			}
			catch (NumberFormatException error){
				System.out.println("Please enter numbers, not anything else!");
			}
		}
		return amountOfPlayer;
	}
	//This method takes a "chosen" card from a given player and gives it to another player. 
	public static void stealCards(Player playerToStealFrom, Player stealingPlayer, int indexOfCardToSteal){
		playerToStealFrom.discardPairs();
		stealingPlayer.discardPairs();
		Card tempCard = playerToStealFrom.getCard(indexOfCardToSteal);
		playerToStealFrom.removeCard(indexOfCardToSteal);
		stealingPlayer.addCard(tempCard);
	}
}