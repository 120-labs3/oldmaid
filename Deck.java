import java.util.Random;
public class Deck{
	private DynamicCardArray deckOfCards; //Already an array
	private boolean isJoker = true;
	Random rand = new Random();

	//<<Constructor>>
	public Deck(){
		this.deckOfCards = new DynamicCardArray();
		//It first fills in all the cards that are not joker {1-52}
		for(Suit suits : Suit.values()){
			for(Value value : Value.values()){
				if(suits == Suit.NONE || value == Value.NONE){
					continue;
				}
				this.deckOfCards.addCard(new Card(suits,value));
			}
		}
		//Fills in the Jokers {53-54}
		for(Joker joker : Joker.values()){
			for(Colors color : Colors.values()){
				if(joker == Joker.NONE || color == Colors.NONE){
					continue;
				}
				this.deckOfCards.addCard(new Card(joker,color,isJoker));
			}
		}	
	}
	//This method returns a specific card at a given index
	public Card getCard(int index){
		return deckOfCards.getCard(index);
	}
	//This method returns the size of the deck
	public int sizeOfDeck(){
		return deckOfCards.getCardCounter();
	}
	//This method shuffles the whole deck
	public void shuffle(){
		for(int i = deckOfCards.getCardCounter() - 1;i>=0;i--){
			int randomPosition = rand.nextInt(i + 1);
			Card tempCard = deckOfCards.getCard(randomPosition);
			Card currTempCard = deckOfCards.getCard(i);
			deckOfCards.replaceCard(tempCard, i);
			deckOfCards.replaceCard(currTempCard, randomPosition);;
		}
	}
	public String toString(){
		String eachCard = deckOfCards.toString();
		return eachCard;
	}
}

