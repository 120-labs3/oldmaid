public class DynamicCardArray{
	private Card[] cards;
	private int cardCounter;
	
	public DynamicCardArray(){
		this.cards = new Card[100];
		this.cardCounter = 0;
	}
	//This method returns a card at a given index
	public Card getCard(int index){
		return cards[index];
	}
	//This method returns the total amount of cards 
	public int getCardCounter(){
		return cardCounter;
	}
	//This method add a specific card to the hand/deck
	public void addCard(Card card){ //For the initial hands
		this.cards[cardCounter] = card;
		this.cardCounter++;
	}
	//This method replaces a card at a given index
	public void replaceCard(Card card, int index){
		this.cards[index] = card;
	}
	//This method removes a card at a given index
	public void removeCard(int index){
		//Resizes the Dynamic card array (or in this game the hand) by removing a specific value
		for(int i = index;i<this.cardCounter;i++){
			this.cards[i] = this.cards[i+1];
		}
		this.cardCounter--;
	}
	public String toString(){
		String handOfCards="";
		for(int i=0;i<cardCounter;i++){
			handOfCards += this.cards[i] + "\n";
		}
		return handOfCards;
	}
}