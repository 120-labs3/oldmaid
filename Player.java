public class Player{
	private DynamicCardArray hand;
	private String name;
	
	public Player(String name){
		this.hand = new DynamicCardArray();
		this.name = name;
	}
	//This method returns a specific card at a given index
	public Card getCard(int index){
		return hand.getCard(index);
	}
	//This method returns the amount of card a player currently has in their hand
	public int getAmountOfCards(){
		return hand.getCardCounter();
	}
	//This method returns the name of a player
	public String getName(){
		return this.name;
	}
	//This method adds a card to the player's hand
	public void addCard(Card card){
		this.hand.addCard(card);
	}
	//This method removes a card from the player's hand
	public void removeCard(int index){
		this.hand.removeCard(index);
	}
	//This method discards all the pairs that are currently present in the player's hand
	public void discardPairs(){
		for(int i = 0;i<this.hand.getCardCounter();i++){ //For every card in the hand
			for(int j = i+1;j<this.hand.getCardCounter();j++){
				if(this.hand.getCard(i).getValue() == Value.NONE && this.hand.getCard(j).getValue() == Value.NONE){
					continue;
				}
				if(this.hand.getCard(i).getValue() == this.hand.getCard(j).getValue()){
					this.hand.removeCard(j);
					this.hand.removeCard(i);
					i = 0;
					j= 0;
				}
			}
		}
	}
	// public void discardPairs(){
	// 	while(getPairs() > 0){
	// 		// if(getPairs() == -1){
	// 		// 	throw new ArrayIndexOutOfBoundsException("No pairs Found");
	// 		// }
	// 		this.hand.removeCard(getPairs());
	// 		this.hand.removeCard(0);
	// 		System.out.println("New hand" + this.hand);
	// 	}
	// }
	// public int getPairs(){
	// 	int pair = -1;
	// 	int skipJoker =0;
	// 	for(int i = 1;i<this.hand.getCardCounter();i++){
	// 		if(this.hand.getCard(skipJoker).getValue() == Value.NONE)
	// 			skipJoker++; 
	// 		if(this.hand.getCard(0).getValue() == Value.NONE && this.hand.getCard(i).getValue() == Value.NONE){
	// 			continue;
	// 		}
	// 		if(this.hand.getCard(skipJoker).getValue() == this.hand.getCard(i).getValue()){
	// 			pair = i;
	// 			break;
	// 		}
	// 	}
	// 	return pair;
	// }
	public String toString(){
		String eachCard = hand.toString();
		return eachCard;
	}
}