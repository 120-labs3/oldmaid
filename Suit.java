public enum Suit{
	HEART{public String toString(){
		return " ♥ ";
	}
	},
	CLOVER{public String toString(){
		return " ♣ ";
	}
	},
	SPADES{public String toString(){
		return " ♠ ";
	}
	},
	DIAMOND{public String toString(){
		return " ♦ ";
	}
	},
	NONE
}