public class DynamicPlayerArray{
    private Player[] players;
    private int playerCount;

    public DynamicPlayerArray(){
        this.players = new Player[20];
        this.playerCount = 0;
    }
	//This method returns a player at a given index
	public Player getPlayer(int index){
		return players[index];
	}
	//This method returns the amount of players
    public int getPlayerCount(){
		return playerCount;
	}
	//This method adds a player in the game
    public void addPlayer(Player player){ //For the initial hands
		this.players[playerCount] = player;
		this.playerCount++;
	}
	//This method removes a player at a given index
    public void removePlayer(int index){
		//Resizes the Dynamic card array (or in this game the hand) by removing a specific value
		for(int i = index;i<this.playerCount;i++){
			this.players[i] = this.players[i+1];
		}
		this.playerCount--;
	}
	public String toString(){
		String contestants="";
		for(int i=0;i<playerCount;i++){
			contestants += this.players[i] + "\n";
		}
		return contestants;
	}
}