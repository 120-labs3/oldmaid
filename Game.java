import java.util.InputMismatchException;
import java.util.Scanner;
public class Game{
	public static void main(String [] args){
		Scanner input = new Scanner(System.in);
		//validates input for the players range
		boolean validValue = false;
		int amountOfPlayer = 0;
		while(!validValue){
			try{
				System.out.println("Please enter the amount of players that will be playing the game. There has to be a minimum of 3 players and a max of 7!");
				amountOfPlayer = Integer.parseInt(input.nextLine()); // Catch and thrown exception if the input is not a int.
				if(amountOfPlayer < 3 || amountOfPlayer > 7){
					System.out.println("Enter the correct range of value");					
				}
				else{
					System.out.println("Thank you for your input! We shall proceed further.");
					validValue = true;
				}
			}
			catch (NumberFormatException error){
				System.out.println("Please enter numbers, not anything else!");
			}
		}
		//Creates a new deck
		Deck deck = new Deck();
		deck.shuffle();
		//Creates the multiple players
		DynamicPlayerArray player = new DynamicPlayerArray();
		for(int i = 0;i <amountOfPlayer;i++){
			player.addPlayer(new Player());
		}
		System.out.println("The player count is " + player.getPlayerCount());
		//Distributes the cards to each player
		int playerCount = 0;
		for(int i = 0;i<deck.sizeOfDeck();i++){
			if(playerCount >= player.getPlayerCount()){
				playerCount = 0;
			}
			player.getPlayer(playerCount).addCard(deck.getCard(i));
			playerCount++;
		} 
		//Start of the game
		boolean gameOver = false;
		int participant = 0;
		while(!gameOver){
				System.out.println("Player " + participant + "'s hand \n"+player.getPlayer(participant));
				player.getPlayer(participant).discardPairs();
				System.out.println("Discarded cards \n Player " + participant + "'s hand \n"+player.getPlayer(participant));
				boolean validCardIndex = false;
				int cardToSteal = 0;
				while(!validCardIndex){
					try{
						System.out.println("Which card would you like to steal? \n Enter a value from 0 to " + (player.getPlayer(participant).getAmountOfCards()-1));
						cardToSteal = Integer.parseInt(input.nextLine());
						if(cardToSteal < 0 || cardToSteal > player.getPlayer(participant).getAmountOfCards()-1){
							System.out.println("Please enter the correct range of value");					
						}
						else{
							System.out.println("Thank you for your input! We shall proceed further.");
							validCardIndex = true;
						}
					}
					catch (NumberFormatException error){
						System.out.println("Please enter numbers, not anything else!");
					}
				}
				//Sets the current player to a variable. Sets the participant back to the first in order for the last player to be able to steal card from the next (first) player
				int currentParticipant = participant;
				if(participant >= player.getPlayerCount() -1){
					participant = -1;
				}
				stealCards(player.getPlayer(currentParticipant),player.getPlayer(participant + 1), cardToSteal);
				player.getPlayer(currentParticipant).discardPairs();
				if(player.getPlayer(currentParticipant).getAmountOfCards() == 0){
					System.out.println("Player " + currentParticipant + " has won!!");
					player.removePlayer(currentParticipant);
					System.out.println("There are now " + player.getPlayerCount() + " amount of players");
				}
				if(player.getPlayerCount() == 0){
					System.out.println("The loser is " + currentParticipant);
					gameOver=true;
				}
				if(participant >= player.getPlayerCount() -1){
					participant = -1;
				}
				participant++;
		}
	}
	//This method takes a "chosen" card from a given player and gives it to another player. 
	public static void stealCards(Player playerToStealFrom, Player stealingPlayer, int indexOfCardToSteal){
		Card tempCard = playerToStealFrom.getCard(indexOfCardToSteal);
		playerToStealFrom.removeCard(indexOfCardToSteal);
		stealingPlayer.addCard(tempCard);
	}
}