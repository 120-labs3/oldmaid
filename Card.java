public class Card{
	
	private Value value;
	private Suit suit;
	private Joker joker;
	private Colors colors;
	private Boolean fancy;
	
	// Cards 1-52
	public Card(Suit suit,Value value){
		this.value = value;
		this.suit = suit;
		this.joker = Joker.NONE;
		this.colors = Colors.NONE;
		this.fancy = false;
	}
	// Joker Cards
	public Card(Joker joker,Colors colors,Boolean isJoker){
		this.value = Value.NONE;
		this.suit = Suit.NONE;
		this.joker = joker;
		this.colors = colors;
		this.fancy = false;
	}
	//This method returns a the enum value
	public Value getValue(){
		return this.value;
	}
	public void setFancy(boolean fancy){
		this.fancy = fancy;
	}
	public String toString(){
		String card ="";
		if(!this.fancy){
			if(this.joker == Joker.NONE){
				card = " " + this.suit + this.value + " ";
			}
			else{
				card = " " + this.joker + this.colors + " ";
			}
			return card;
		}
		else {
			if(this.joker == Joker.NONE){
				card = " _ _ _ _ _ \n|" + this.suit + "       |\n|          |\n|          |\n|   " + this.value +"    |\n|          |\n|          |\n|      "+this.suit+" |\n|_ _ _ _ _ |";
			}
			else{
				card = " _ _ _ _ _ \n|" + this.colors + "       |\n|          |\n|          |\n|  " + this.joker +"   |\n|          |\n|          |\n|      "+this.colors+" |\n|_ _ _ _ _ |";
			}
			return card;
		}
	}
}