import java.util.InputMismatchException;
import java.util.Scanner;
public class test{
    public static void main(String [] args){
        Scanner input = new Scanner(System.in);

        System.out.println("creating and shuffling the deck");
		Deck deck = new Deck();
		deck.shuffle();

        System.out.println("creating the players and distributing the cards");
        DynamicPlayerArray player = new DynamicPlayerArray();
		for(int i = 0;i <1;i++){
			player.addPlayer(new Player());
		}
        int playerCount = 0;
		for(int i = 0;i<deck.sizeOfDeck();i++){
			if(playerCount >= player.getPlayerCount()){
				playerCount = 0;
			}
			player.getPlayer(playerCount).addCard(deck.getCard(i));
			playerCount++;
		} 
        System.out.println("Player's hands \n"+player.getPlayer(0));
        System.out.println("discarding pairs");
        player.getPlayer(0).discardPairs();
        System.out.println("Player's parsed hand \n" + player.getPlayer(0));
    }
}